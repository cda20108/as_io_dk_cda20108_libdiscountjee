<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet" href="assets/css/main.css">
<title>Incription</title>
</head>
<body>

	
		<h1>Inscription</h1>

		<div class="formContainer">

			<form action="inscription" method="post" class="formInscription">

				<label for="Nom">Nom</label>
			    <input type="text" name="Nom" required="required"> 
			    
			    <label for="Prenom">Prenom</label> 
			    <input type="text" name="Prenom" required="required"><br> 
			    
			    <label for="Mail">Mail</label> 
			    <input type="text" name="Mail"required="required">
			    
			    <label for="Tel">Tel</label> 
			    <input type="text" name="Tel" required="required"><br>
			    
				<hr>
				
				<label for="NomLib">Nom Librairie</label> 
				<input type="text" name="NomLib" required="required"><br>
				
		        <label for="NumRue">Numero de rue</label> 
		        <input type="text" name="NumRue" required="required">
		        
		        <label for="NomRue">Nom rue</label> 
		        <input type="text" name="NomRue" required="required"><br> 
		        
		        <label for="CP">Code Postal</label>
		        <input type="text" name="CP" required="required"> 
		        
		        <label for="Ville">Ville</label> 
		        <input type="text" name="Ville" required="required"><br>
		        
		        <label for="pwd">Mot de passe</label> 
		        <input type="password" name="pwd" required="required">
		        
		        <label for="pwdc">Confirmation de Mot de passe</label>
		        <input type="password" name="pwdc" required="required"><br>
		        
				<hr>
				
				<button type="submit">Valider</button>

			</form>


		</div>

</body>
</html>