<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
</head>
<body>

	<h1>Login</h1>

	<div>
		<form action="Login" method="post">
			<label for="login">Votre Identifiant</label> <input type="text"
				placeholder="Entrez votre login" name="login" required="required">
			<label for="pwd">Votre mot de passe</label> <input type="password"
				placeholder="Entrez votre mot de passe" name="mdp"
				required="required">
			<button type="submit">Se Connecter</button>
		</form>
		
		<a href="inscription"> Pas encore inscrit ?</a>
	</div>
	
	<div class="imgLeft">
		<p>Vos livres aux meilleurs prix</p>
	</div>
</body>
</html>
