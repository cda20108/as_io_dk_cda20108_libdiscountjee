package fr.afpa.dao;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.Transaction;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Utilisateur;
import fr.afpa.utils.HibernateUtils;

public class DaoUtilisateur {
	
	private static  Session session = null;
	 
	public void addUser( Utilisateur user, Adresse adresse, Compte compte) {

	
	session = HibernateUtils.getSession();
	
	Transaction tx =session.beginTransaction();

	session.save(user);
	tx.commit();
	session.close();
	
	}
	
	public void updateUtilisateur(Utilisateur user) {
		session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();
		
		session.update(user.getAdresse());
		session.update(user.getCompte());
		session.update(user);

		tx.commit();
		session.close();
	}
	
	public int findUserByLoginAndPassword(String login, String mdp) {
		
		session = HibernateUtils.getSession();
		Transaction tx = session.beginTransaction();
		
		Query req = session.getNamedQuery("findUserByLoginAndPwd");
		req.setParameter("login", login);
		req.setParameter("mdp", mdp);
		
		int id = (int) req.getSingleResult();
		
		return id;
	}

}
