package fr.afpa.dao;

import java.util.List;

import javax.persistence.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import fr.afpa.beans.Annonce;
import fr.afpa.utils.HibernateUtils;

public class DaoAnnonce {

	public void addAnnonce(Annonce annonce) {

		Session session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		session.persist(annonce);

		tx.commit();
		session.close();
	}
	
	
	
	public void updateAnnonce(Annonce annonce) {

		Session session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		session.update(annonce);

		tx.commit();
		session.close();
	}
	
	public void supprimerAnnonce(Annonce annonce) {

		Session session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		session.delete(annonce);

		tx.commit();
		session.close();
	}
	
	public List<Annonce> listerAnnonce() {

		Session session = HibernateUtils.getSession();

		Transaction tx = session.beginTransaction();

		Query req = session.createQuery("from Utilisateur ");

		List<Annonce> listeAnnonce = (List<Annonce>) req.getResultList();

		tx.commit();
		session.close();

		return listeAnnonce;

	}
	
	
}


