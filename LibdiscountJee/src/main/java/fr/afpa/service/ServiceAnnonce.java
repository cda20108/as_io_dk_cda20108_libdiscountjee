package fr.afpa.service;

import java.util.List;

import fr.afpa.beans.Annonce;
import fr.afpa.dao.DaoAnnonce;

public class ServiceAnnonce {
	
	private DaoAnnonce daoA = new DaoAnnonce();
	
	public void addAnnonce(Annonce annonce) {
		
		daoA.addAnnonce(annonce);
		
	}
	
	public void updateAnnonce(Annonce annonce) {
		
		daoA.updateAnnonce(annonce);
		
	}
	public void deleteAnnonce(Annonce annonce) {
		
		daoA.supprimerAnnonce(annonce);
		
	}
	public List<Annonce> listerAnnonce() {
		
		return daoA.listerAnnonce();
		
	}

}
