package fr.afpa.service;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.DaoUtilisateur;

public class ServiceUtilisateur {

	DaoUtilisateur daoU = new DaoUtilisateur();

	public void addUser(Utilisateur user, Adresse adresse, Compte compte) {
		daoU.addUser(user, adresse, compte);
	}

	public void updateUser(Utilisateur user) {
		daoU.updateUtilisateur(user);
	}
	
	public int findUserByLoginAndPassword(String login , String mdp) {
		
		return daoU.findUserByLoginAndPassword(login, mdp);
	}

}
