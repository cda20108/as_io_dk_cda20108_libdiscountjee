package fr.afpa.beans;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@RequiredArgsConstructor
public class Adresse {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_adresse")
    private int id;

	@NonNull
	private String numRue;
	@NonNull
	private String nomRue;
	@NonNull
	private String codeP;
	@NonNull
	private String ville;
	
}
