package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@NamedQueries({
		@NamedQuery(name = "findUserByLoginAndPwd", query = " Select u.id From Utilisateur as u where u.compte.login =:login and u.compte.mdp =:mdp") })
public class Utilisateur {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	@NonNull
	private String nom;
	@NonNull
	private String prenom;
	@NonNull
	private String nomLib;
	@NonNull
	private String mail;
	@NonNull
	private String tel;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_compte")
	private Compte compte;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_adresse")
	private Adresse adresse;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "utilisateur")
	private List<Annonce> listAnnonce;
	@NonNull
	private Boolean adm;
}
