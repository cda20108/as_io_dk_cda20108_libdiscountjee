package fr.afpa.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Annonce {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id; 
	
	private String titre;
	private String isbn;
	private LocalDate dateEdition;
	private String maisonEdition;
	private double prixUni;
	private int quantite;
	private double remise;
	private LocalDate dateAnnonce;
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "utilisateur")
	private Utilisateur utilisateur;
	@OneToMany(cascade = CascadeType.ALL, mappedBy="annonce")
	private List<Image> imgAnnonce; 
	
}
