package fr.afpa.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.afpa.beans.Adresse;
import fr.afpa.beans.Compte;
import fr.afpa.beans.Utilisateur;
import fr.afpa.service.ServiceUtilisateur;

/**
 * Servlet implementation class Inscription
 */
public class Inscription extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private ServiceUtilisateur su = new ServiceUtilisateur();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Inscription() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		
		Compte compte = new Compte(request.getParameter("pwd"), request.getParameter("Mail"));
		
		Adresse adresse = new Adresse(request.getParameter("NumRue"), request.getParameter("NomRue"),
					      request.getParameter("CP"), request.getParameter("Ville"));

		Utilisateur user = new Utilisateur(request.getParameter("Nom"), request.getParameter("Prenom"),
						   request.getParameter("NomLib"), request.getParameter("Mail"), request.getParameter("Tel"), false);
	
		user.setAdresse(adresse);
		user.setCompte(compte);
		
		su.addUser(user, adresse, compte);
		
		response.sendRedirect("index.jsp");
		
		

	}

}
